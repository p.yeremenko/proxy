package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

var (
	m = make(map[string]*httputil.ReverseProxy)
)

func main() {
	port := getPortOrDefault(":8090")
	prefix := getPrefix()
	defaultScheme := getSchemeOrDefault("http")

	log.WithFields(log.Fields{"prefix": prefix, "port": port, "defaultScheme": defaultScheme}).
		Info("starting the proxy")

	http.HandleFunc("/health", healthCheck)
	http.HandleFunc("/", handler(prefix, defaultScheme))
	err := http.ListenAndServe(port, nil)
	if err != nil {
		panic(err)
	}
}

func handler(prefix, defaultScheme string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		scheme := defaultScheme
		if r.URL.Scheme != "" {
			scheme = r.URL.Scheme
		}

		targetHost, targetPath, err := getTargetHost(r, prefix, scheme)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		log.WithFields(log.Fields{"host": targetHost, "origin-url": r.URL.RequestURI()}).Info("making a proxy")

		p, err := makeProxy(targetHost)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		targetURL, err := url.Parse(targetHost + targetPath)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("failed to make a target URL"))
			return
		}

		newR := *r
		newR.Host = targetURL.Host
		newR.RequestURI = targetPath

		u := *newR.URL
		u.Path = targetURL.Path
		u.RawQuery = targetURL.RawQuery
		u.Scheme = targetURL.Scheme
		u.Host = targetURL.Host
		u.Path = targetURL.Path

		newR.URL = &u

		p.ServeHTTP(w, &newR)
	}
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Ok."))
	w.WriteHeader(http.StatusOK)
}

func getTargetHost(r *http.Request, prefix, scheme string) (string, string, error) {
	fullPath := fmt.Sprintf("%s?%s", r.URL.Path, r.URL.RawQuery)
	if prefix != "" {
		schemaless := strings.Join(strings.Split(fullPath, prefix)[1:], prefix)

		u, err := url.Parse(fmt.Sprintf("%s://%s", scheme, schemaless))
		if err != nil {
			return "", "", err
		}

		return fmt.Sprintf("%s://%s", u.Scheme, u.Host), u.RequestURI(), nil
	} else {
		host := "localhost"
		hosts := r.Header["X-Forwarded-Host"]
		if len(hosts) > 0 {
			host = hosts[0]
		}

		u, err := url.Parse(fmt.Sprintf("%s://%s/%s", scheme, host, fullPath))
		if err != nil {
			return "", "", err
		}

		return fmt.Sprintf("%s://%s", u.Scheme, u.Host), u.RequestURI(), nil
	}
}

func makeProxy(proxyURL string) (*httputil.ReverseProxy, error) {
	readyProxy, ok := m[proxyURL]
	if ok {
		return readyProxy, nil
	}

	remote, err := url.Parse(proxyURL)
	if err != nil {
		return nil, err
	}

	proxy := httputil.NewSingleHostReverseProxy(remote)
	// TODO: use an LRU cache in case of performance issue
	// 	     or at least a map: m[proxyURL] = proxy

	return proxy, nil
}

func getPortOrDefault(defaultPort string) string {
	value := os.Getenv("PROXY_PORT")
	if value == "" {
		value = defaultPort
	}
	if !strings.HasPrefix(value, ":") {
		value = ":" + value
	}

	return value
}

func getPrefix() string {
	value := os.Getenv("PROXY_PREFIX")
	if value == "" {
		return ""
	}
	if !strings.HasSuffix(value, "/") {
		value = value + "/"
	}

	return value
}

func getSchemeOrDefault(defaultScheme string) string {
	value := os.Getenv("PROXY_SCHEME")
	if value == "" {
		return defaultScheme
	}

	return value
}
